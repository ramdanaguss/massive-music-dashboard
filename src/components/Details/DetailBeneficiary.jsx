'use client';

import DetailCard from '@/components/Details/DetailCard';
import DetailCardItem from '@/components/Details/DetailCardItem';

import { useIsClient } from '@/hooks/useIsClient';
import styles from '@/components/Details/DetailBeneficiary.module.scss';
import cardStyles from '@/components/Details/DetailCard.module.scss';

function DetailBeneficiary({ detail }) {
  const isClient = useIsClient();

  const address = detail.address.split(', ');

  if (!isClient) return null;

  return (
    <div className={styles['detail-beneficiary']}>
      <hr className={styles['detail-beneficiary--underline']} />

      <DetailCard title="Beneficiary">
        <div className={cardStyles['detail-card-body-beneficiary']}>
          <DetailCardItem label="Name">{detail.name}</DetailCardItem>

          <DetailCardItem label="E-mail">{detail.email}</DetailCardItem>

          <DetailCardItem label="Birth Place/Date">
            {detail.birthPlaceDate}
          </DetailCardItem>

          <DetailCardItem label="Account No">{detail.birthPlaceDate}</DetailCardItem>

          <DetailCardItem label="Address">
            <p className={cardStyles['detail-address']}>
              {address.map((subset) => (
                <span key={subset}>{subset}</span>
              ))}
            </p>
          </DetailCardItem>

          <DetailCardItem label="Bank Name">{detail.bankName}</DetailCardItem>

          <DetailCardItem label="Account Name">{detail.accountName}</DetailCardItem>

          <DetailCardItem label="Territories">{detail.territories}</DetailCardItem>
        </div>
      </DetailCard>
    </div>
  );
}

export default DetailBeneficiary;
