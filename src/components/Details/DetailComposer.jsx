'use client';

import DetailCard from '@/components/Details/DetailCard';
import DetailCardItem from '@/components/Details/DetailCardItem';

import styles from '@/components/Details/DetailComposer.module.scss';
import cardStyles from '@/components/Details/DetailCard.module.scss';

function DetailComposer({ detail }) {
  return (
    <div className={styles['detail-composer']}>
      <DetailCard title="Composer Detail">
        <div className={cardStyles['detail-card-body']}>
          <DetailCardItem label="Aka(s)">{detail.aka}</DetailCardItem>

          <DetailCardItem label="Gender">{detail.gender}</DetailCardItem>

          <DetailCardItem label="Register Date">{detail.registerDate}</DetailCardItem>

          <DetailCardItem label="IPI Number">{detail.IPINumber}</DetailCardItem>

          <DetailCardItem label="Perfomance Society Affiliations">
            {detail.perfomanceSocietyAffiliations}
          </DetailCardItem>

          <DetailCardItem label="Mechanical Society Affiliations">
            {detail.mechanicalSocietyAffiliations}
          </DetailCardItem>
        </div>
      </DetailCard>
    </div>
  );
}

export default DetailComposer;
