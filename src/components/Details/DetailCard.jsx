import { Card } from 'react-bootstrap';
import styles from '@/components/Details/DetailCard.module.scss';

function DetailCard({ title, children }) {
  const cardClasses = `${styles['detail-card']}`;

  return (
    <Card className={cardClasses}>
      <Card.Title>{title}</Card.Title>
      <Card.Body>{children}</Card.Body>
    </Card>
  );
}

export default DetailCard;
