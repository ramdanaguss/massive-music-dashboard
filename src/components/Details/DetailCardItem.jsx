'use client';

import styles from '@/components/Details/DetailCardItem.module.scss';

function DetailCardItem({ label, children }) {
  return (
    <div
      className={styles['detail-card-item']}
      style={label === 'Address' ? { gridRow: '3 / 6' } : {}}
    >
      <p style={label === 'Address' ? { paddingBottom: '35px' } : {}}>{label}</p>
      <span>{children}</span>
    </div>
  );
}

export default DetailCardItem;
