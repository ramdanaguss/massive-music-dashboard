import DetailBeneficiary from '@/components/Details/DetailBeneficiary';
import DetailComposer from '@/components/Details/DetailComposer';

import { getDetailBeneficiary, getDetailComposer } from '@/service/details';
import styles from '@/components/Details/Details.module.scss';

async function Details() {
  const detailComposer = await getDetailComposer();
  const detailBeneficiary = await getDetailBeneficiary();

  return (
    <section className={styles.details}>
      <DetailComposer detail={detailComposer} />
      <DetailBeneficiary detail={detailBeneficiary} />
    </section>
  );
}

export default Details;
