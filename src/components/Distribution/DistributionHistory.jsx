import CustomTable from '@/components/CustomTable/CustomTable';
import DistributionHistoryList from '@/components/Distribution/DistributionHistoryList';

import { getDistributionHistory } from '@/service/distribution';

async function DistributionHistory() {
  const distributionHistory = await getDistributionHistory();

  return (
    <section>
      <CustomTable
        title="Distribution History"
        hasButtons
        hasFooter
        tableHead={[
          'Accounting Period',
          'Opening Balance',
          'Payment',
          'Royalty',
          'Closing Balance',
        ]}
      >
        <DistributionHistoryList distributionHistory={distributionHistory} />
      </CustomTable>
    </section>
  );
}

export default DistributionHistory;
