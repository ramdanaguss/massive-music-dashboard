function DistributionHistoryList({ distributionHistory = [] }) {
  return distributionHistory.map((item, index) => (
    <tr key={item.id} colored={(index + 1) % 2 !== 0 ? 'true' : 'false'}>
      <td>{item.accountingPeriod}</td>
      <td>{item.openingBalance}</td>
      <td>{item.payment}</td>
      <td>{item.royalty}</td>
      <td>{item.closingBalance}</td>
    </tr>
  ));
}

export default DistributionHistoryList;
