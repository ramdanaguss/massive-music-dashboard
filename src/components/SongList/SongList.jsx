import { getSongList } from '@/service/songList';
import CustomTable from '@/components/CustomTable/CustomTable';
import SongListItem from '@/components/SongList/SongListItem';

async function SongList() {
  const songList = await getSongList();

  return (
    <section>
      <CustomTable
        title="Song List"
        tableHead={['Song Title', 'Release Date', 'ISWC', 'Total Stream']}
      >
        <SongListItem songList={songList} />
      </CustomTable>
    </section>
  );
}

export default SongList;
