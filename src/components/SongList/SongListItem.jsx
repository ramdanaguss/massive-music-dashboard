'use client';

import { useState, Fragment } from 'react';
import { Table } from 'react-bootstrap';

function SongListItem({ songList = [] }) {
  const [currentActive, setCurrentActive] = useState(6);

  function click(id) {
    return () => {
      setCurrentActive((currentActive) => {
        if (currentActive === id) return '';

        return id;
      });
    };
  }

  return songList.map((item, index) => (
    <Fragment key={item.id}>
      <tr
        className="collapsable-trigger"
        onClick={click(item.id)}
        colored={(index + 1) % 2 !== 0 ? 'true' : 'false'}
      >
        <td>
          <span
            className="icon-chevron-right"
            style={{ transform: currentActive === item.id ? 'rotate(90deg)' : '' }}
          />
          <span>{item.songTitle}</span>
        </td>
        <td>{item.releaseDate}</td>
        <td>{item.ISWC}</td>
        <td>{item.totalStream}</td>
      </tr>

      {currentActive === item.id && (
        <tr
          className="collapsable-content"
          colored={(index + 1) % 2 !== 0 ? 'true' : 'false'}
        >
          <td colSpan={4}>
            <div
              className="collapsable-content-table-wrapper"
              style={{
                backgroundColor: (index + 1) % 2 !== 0 ? 'white' : '#f1f1f1',
              }}
            >
              <Table responsive>
                <thead>
                  <tr>
                    <th>Title</th>
                    <th>ISRC</th>
                    <th>Total Stream</th>
                  </tr>
                </thead>

                <tbody>
                  {item.detail.map((detailItem) => (
                    <tr key={detailItem.id}>
                      <td>
                        <div>
                          <span className="icon-chevron-right" />
                        </div>

                        <div>
                          <span>{detailItem.title.songTitle}</span>

                          <div>
                            <span>{detailItem.title.singer}</span>
                            &middot;
                            <span>{detailItem.title.album}</span>
                          </div>
                        </div>
                      </td>

                      <td>{detailItem.ISRC}</td>

                      <td>{detailItem.totalStream}</td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </div>
          </td>
        </tr>
      )}
    </Fragment>
  ));
}

export default SongListItem;
