'use client';

import { Button } from 'react-bootstrap';

import styles from '@/components/Headline/HeadlineButton.module.scss';
import { useIsClient } from '@/hooks/useIsClient';

function HeadlineButton({ icon, children, active }) {
  const isClient = useIsClient();

  if (!isClient) return null;

  return (
    <>
      <style type="text/css">
        {`
          .btn-headline {
            background-color: #ffffff !important;
            
            font-weight: 500;
            border-radius: 1000px;
            display: flex;
            gap: 18px;
            align-items: center;
            padding: 13px 20px
          }

          .btn-headline > span::before {
            font-size: 24px;
          }
        `}
      </style>

      <Button
        variant="headline"
        className={`${styles['headline-button']}`}
        style={{ border: active ? '1px solid #8758ff' : '0' }}
      >
        <span className={icon} />
        <span>{children}</span>
      </Button>
    </>
  );
}

export default HeadlineButton;
