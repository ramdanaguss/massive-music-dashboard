import HeadlineOperations from '@/components/Headline/HeadlineOperations';
import HeadlineTitle from '@/components/Headline/HeadlineTitle';

import styles from '@/components/Headline/Headline.module.scss';

function Headline() {
  return (
    <section className={styles.headline}>
      <HeadlineTitle />
      <HeadlineOperations />
    </section>
  );
}

export default Headline;
