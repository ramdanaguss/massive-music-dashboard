import HeadlineDate from '@/components/Headline/HeadlineDate';
import HeadlineFilter from '@/components/Headline/HeadlineFilter';

import styles from '@/components/Headline/HeadlineOperations.module.scss';

function HeadlineOperations() {
  return (
    <div className={styles['headline-operations']}>
      <HeadlineFilter />
      <HeadlineDate />
    </div>
  );
}

export default HeadlineOperations;
