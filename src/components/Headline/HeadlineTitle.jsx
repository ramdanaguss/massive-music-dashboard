import { abeezee } from '@/font/font';
import styles from '@/components/Headline/HeadlineTitle.module.scss';

function HeadlineTitle() {
  return (
    <div className={styles['headline-title']}>
      <h1>Baskara Putra</h1>
      <p className={`${abeezee.className}`}>Last updated at 9 October 2023</p>
    </div>
  );
}

export default HeadlineTitle;
