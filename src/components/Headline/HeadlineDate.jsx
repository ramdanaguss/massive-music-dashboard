import HeadlineButton from '@/components/Headline/HeadlineButton';

function HeadlineDate() {
  return (
    <HeadlineButton icon="icon-calendar" active>
      January, 2023
    </HeadlineButton>
  );
}

export default HeadlineDate;
