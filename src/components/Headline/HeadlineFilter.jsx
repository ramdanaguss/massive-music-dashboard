import HeadlineButton from '@/components/Headline/HeadlineButton';

function HeadlineFilter() {
  return (
    <HeadlineButton icon="icon-filter" active={false}>
      Filter By
    </HeadlineButton>
  );
}

export default HeadlineFilter;
