'use client';

import { Form, InputGroup } from 'react-bootstrap';

import { abeezee } from '@/font/font';
import styles from '@/components/SearchBar/SearchBar.module.scss';

function SearchBar() {
  return (
    <InputGroup className={`${styles['search-bar']} ${abeezee.className}`}>
      <InputGroup.Text
        as="label"
        htmlFor="search"
        className="icon-search"
      ></InputGroup.Text>

      <Form.Control
        id="search"
        placeholder="Search..."
        type="text"
        aria-describedby="search"
      />
      <InputGroup.Text id="voice" className="icon-microphone"></InputGroup.Text>
    </InputGroup>
  );
}

export default SearchBar;
