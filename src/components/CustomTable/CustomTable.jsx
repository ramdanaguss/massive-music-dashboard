'use client';

import { useState } from 'react';
import { Button, Table } from 'react-bootstrap';

import { abeezee } from '@/font/font';
import styles from '@/components/CustomTable/CustomTable.module.scss';

function CustomTable({
  title = 'Title',
  hasButtons = false,
  hasFooter = false,
  tableHead = [],
  children,
}) {
  const [activeButton, setActiveButton] = useState('H');

  function HClick() {
    setActiveButton('H');
  }

  function QClick() {
    setActiveButton('Q');
  }

  return (
    <div className={styles['custom-table']}>
      <div className={styles['custom-table-header']}>
        <di className={styles['custom-table-header--title']}>
          <h4>{title}</h4>
          {hasButtons && (
            <div className={styles['custom-table-header--buttons']}>
              <>
                <style type="text/css">{`
                  .btn-rounded {
                    background-color: #F6F6F6;
                    border-radius: 1000px;
                    font-weight: 500;
                  }
                  
                  .btn-rounded:hover {
                    background-color: #F6F6F6;
                  }
                `}</style>

                <Button
                  variant="rounded"
                  style={activeButton === 'H' ? { backgroundColor: '#D9D9D9' } : {}}
                  onClick={HClick}
                >
                  H
                </Button>
                <Button
                  variant="rounded"
                  style={activeButton === 'Q' ? { backgroundColor: '#D9D9D9' } : {}}
                  onClick={QClick}
                >
                  Q
                </Button>
              </>
            </div>
          )}
        </di>

        <>
          <style type="text/css">{`
            .btn-sort {
              background-color: transparent;
              display: flex;
              align-items: center;
              gap: 10px;
              border: 0;
            }
          `}</style>

          <Button variant="sort">
            <span className="icon-sort-down" />
            <span>Sort By</span>
          </Button>
        </>
      </div>

      <Table className={styles['custom-table-content']} responsive>
        <thead>
          <tr>
            {tableHead.map((th) => (
              <th key={th}>{th}</th>
            ))}
          </tr>
        </thead>

        <tbody>{children}</tbody>
      </Table>

      {hasFooter && (
        <div className={styles['custom-table-footer']}>
          <>
            <style type="text/css">{`
            .btn-show-more {
              background-color: #D9D9D9;
              padding: 12px 72px;
              border-radius: 0;
              font-size: 17.5px;
              font-style: italic;
              font-weight: 400;
              line-height: 22.455px;
            }
            
            .btn-show-more:hover {
              background-color: #D9D9D9;
            }
          `}</style>

            <Button variant="show-more" className={abeezee.className}>
              Show More
            </Button>
          </>
        </div>
      )}

      {!hasFooter && <div style={{ padding: '20px' }} />}
    </div>
  );
}

export default CustomTable;
