import StatisticsCard from '@/components/Statistics/StatisticsCard';
import styles from '@/components/Statistics/Statistics.module.scss';

function Statistics() {
  return (
    <section className={styles.statistics}>
      <StatisticsCard title="Total Royalty" body="1.000,25" full />
      <StatisticsCard title="Upcoming Royalty" body="50,25" full />
      <StatisticsCard title="Total Songs" body="98" />
    </section>
  );
}

export default Statistics;
