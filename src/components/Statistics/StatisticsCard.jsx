'use client';

import { Card } from 'react-bootstrap';

import { abeezee } from '@/font/font';
import styles from '@/components/Statistics/StatisticsCard.module.scss';

function StatisticsCard({ title, body, full }) {
  const cardClasses = `${styles['statistics-card']} ${
    full ? styles['statistics-card--full'] : styles['statistics-card--not-full']
  }`;

  return (
    <Card className={cardClasses}>
      <Card.Title>{title}</Card.Title>
      <Card.Body className={abeezee.className}>{body}</Card.Body>
    </Card>
  );
}

export default StatisticsCard;
