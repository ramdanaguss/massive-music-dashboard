import { Montserrat, ABeeZee } from 'next/font/google';

export const montserrat = Montserrat({
  subsets: ['latin'],
});

export const abeezee = ABeeZee({
  subsets: ['latin'],
  weight: '400',
});
