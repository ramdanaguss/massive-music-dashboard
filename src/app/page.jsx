import Headline from '@/components/Headline/Headline';
import Statistics from '@/components/Statistics/Statistics';
import Details from '@/components/Details/Details';
import DistributionHistory from '@/components/Distribution/DistributionHistory';
import SongList from '@/components/SongList/SongList';

function HomePage() {
  return (
    <article>
      <Headline />
      <Statistics />
      <Details />
      <DistributionHistory />
      <SongList />
    </article>
  );
}

export default HomePage;
