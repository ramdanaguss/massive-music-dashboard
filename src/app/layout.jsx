import SearchBar from '@/components/SearchBar/SearchBar';

import { montserrat } from '../font/font';

import './global.scss';
import styles from './homapage.module.scss';

export const metadata = {
  title: 'Massive Music Dashboard',
};

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body className={montserrat.className}>
        <header className={styles.header}>
          <div className={styles['header-container']}>
            <SearchBar />
            <span className="icon-information" />
          </div>
        </header>

        <main className={styles.main}>{children}</main>
      </body>
    </html>
  );
}
