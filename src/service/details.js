import axios from 'axios';

const BASE_URL = process.env.BASE_URL || 'http://localhost:3000';

export async function getDetailComposer() {
  try {
    const { data } = await axios.get(`${BASE_URL}/data/details/composerDummy.json`);

    return data;
  } catch (error) {
    console.error(error);
  }
}

export async function getDetailBeneficiary() {
  try {
    const { data } = await axios.get(`${BASE_URL}/data/details/beneficiaryDummy.json`);

    return data;
  } catch (error) {
    console.error(error);
  }
}
