import axios from 'axios';

const BASE_URL = process.env.BASE_URL || 'http://localhost:3000';

export async function getDistributionHistory() {
  try {
    const { data } = await axios.get(
      `${BASE_URL}/data/distribution/distributionHistoryDummy.json`
    );

    return data;
  } catch (error) {
    console.error(error);
  }
}
