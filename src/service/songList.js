import axios from 'axios';

const BASE_URL = process.env.BASE_URL || 'http://localhost:3000';

export async function getSongList() {
  try {
    const { data } = await axios.get(`${BASE_URL}/data/songList/songList.json`);

    return data;
  } catch (error) {
    console.error(error);
  }
}
